package com.treinamento.estudosprgbootang.dtos;

import java.math.BigDecimal;

public class VeiculosConsultaDTO {
	
	private Integer idVeiculo;
	
	private String descricao;
	
	private BigDecimal valor_veiculo;
	
	private boolean alugado;	

	public VeiculosConsultaDTO(Integer idVeiculo, String descricao, BigDecimal valor_veiculo, boolean alugado) {
		this.idVeiculo = idVeiculo;
		this.descricao = descricao;
		this.valor_veiculo = valor_veiculo;
		this.alugado = alugado;
	}

	public Integer getIdVeiculo() {
		return idVeiculo;
	}

	public void setIdVeiculo(Integer idVeiculo) {
		this.idVeiculo = idVeiculo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getValor_veiculo() {
		return valor_veiculo;
	}

	public void setValor_veiculo(BigDecimal valor_veiculo) {
		this.valor_veiculo = valor_veiculo;
	}

	public boolean isAlugado() {
		return alugado;
	}

	public void setAlugado(boolean alugado) {
		this.alugado = alugado;
	}

}
