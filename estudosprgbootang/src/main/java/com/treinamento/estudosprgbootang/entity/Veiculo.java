package com.treinamento.estudosprgbootang.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "veiculos")
public class Veiculo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id_veiculo;
	
	private BigDecimal valor_veiculo;
	
	private String descricao;
	
	private boolean alugado;

	public Veiculo(BigDecimal valor_veiculo, String descricao, boolean alugado) {
		this.valor_veiculo = valor_veiculo;
		this.descricao = descricao;
		this.alugado = alugado;
	}

	public Veiculo() {
	}

	public BigDecimal getValor_veiculo() {
		return valor_veiculo;
	}

	public void setValor_veiculo(BigDecimal valor_veiculo) {
		this.valor_veiculo = valor_veiculo;
	}

	public boolean isAlugado() {
		return alugado;
	}

	public void setAlugado(boolean alugado) {
		this.alugado = alugado;
	}

	public Integer getId_veiculo() {
		return id_veiculo;
	}

	public void setId_veiculo(Integer id_veiculo) {
		this.id_veiculo = id_veiculo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
