package com.treinamento.estudosprgbootang.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.treinamento.estudosprgbootang.dtos.VeiculosConsultaDTO;
import com.treinamento.estudosprgbootang.entity.Veiculo;

public interface VeiculosRepository extends JpaRepository<Veiculo, Integer>{
	
	@Query("select new com.treinamento.estudosprgbootang.dtos.VeiculosConsultaDTO " +
			   " (v.id, v.descricao, v.valor_veiculo, v.alugado) " +
			   "from Veiculo v " +
			   "order by v.descricao ")
		List<VeiculosConsultaDTO> findAllAsDTO();
	
	@Query("select new com.treinamento.estudosprgbootang.dtos.VeiculosConsultaDTO " +
			   " (v.id, v.descricao, v.valor_veiculo, v.alugado) " +
			   "from Veiculo v " +
			   " where (v.descricao = :descricao) " +
			   "order by v.descricao ")
		List<VeiculosConsultaDTO> findAllByDescricaoAsDTO(@Param("descricao") String descricao);

}
