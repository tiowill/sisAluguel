package com.treinamento.estudosprgbootang.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.treinamento.estudosprgbootang.dtos.VeiculosConsultaDTO;
import com.treinamento.estudosprgbootang.repository.VeiculosRepository;



@RestController
@RequestMapping("/veiculos")
public class VeiculosAPI {

	@Autowired
	public VeiculosRepository veiculosRepository;
	
	@GetMapping
	public List<VeiculosConsultaDTO> getTodos(@RequestParam(value="descricao", required=false) String descricao) {
		if (descricao == null) {
			return this.veiculosRepository.findAllAsDTO();
		} else {
			return this.veiculosRepository.findAllByDescricaoAsDTO(descricao);
		}
	}
}
