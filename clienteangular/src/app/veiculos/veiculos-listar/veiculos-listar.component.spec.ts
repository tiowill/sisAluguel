import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VeiculosListarComponent } from './veiculos-listar.component';

describe('VeiculosListarComponent', () => {
  let component: VeiculosListarComponent;
  let fixture: ComponentFixture<VeiculosListarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VeiculosListarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VeiculosListarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
