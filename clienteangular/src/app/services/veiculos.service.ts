import { Injectable } from '@angular/core';
import Veiculo from '../models/Veiculo';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class VeiculosService {

  constructor(private http:HttpClient) { }

  buscarPorFiltro(descricao: string): Observable<Veiculo[]> {

    if(descricao!=null && typeof descricao != undefined && descricao !=''  ){
      
      let params = new HttpParams();
      params = params.append('descricao', descricao);
      return this.http
      .get(`${environment.apiBaseUrl}veiculos`,{ params })
      .pipe(map(x => <Veiculo[]>x))

    }else{
      return this.http
      .get(`${environment.apiBaseUrl}veiculos`)
      .pipe(map(x => <Veiculo[]>x))
    }
    
  }
  
}
